
//Remplissage du menu
(function() {

	var remplirMenu = function(objetMenu){

		document.querySelector(".lundiContainer .entree").innerText = objetMenu.lundi[0];
		document.querySelector(".lundiContainer .plat").innerText = objetMenu.lundi[1];
		document.querySelector(".lundiContainer .dessert").innerText = objetMenu.lundi[2];
		document.querySelector(".mardiContainer .entree").innerText = objetMenu.mardi[0];
		document.querySelector(".mardiContainer .plat").innerText = objetMenu.mardi[1];
		document.querySelector(".mardiContainer .dessert").innerText = objetMenu.mardi[2];
		document.querySelector(".mercrediContainer .entree").innerText = objetMenu.mercredi[0];
		document.querySelector(".mercrediContainer .plat").innerText = objetMenu.mercredi[1];
		document.querySelector(".mercrediContainer .dessert").innerText = objetMenu.mercredi[2];
		document.querySelector(".jeudiContainer .entree").innerText = objetMenu.jeudi[0];
		document.querySelector(".jeudiContainer .plat").innerText = objetMenu.jeudi[1];
		document.querySelector(".jeudiContainer .dessert").innerText = objetMenu.jeudi[2];
		document.querySelector(".vendrediContainer .entree").innerText = objetMenu.vendredi[0];
		document.querySelector(".vendrediContainer .plat").innerText = objetMenu.vendredi[1];
		document.querySelector(".vendrediContainer .dessert").innerText = objetMenu.vendredi[2];


		document.querySelector(".lundiContainer .entree").textContent = objetMenu.lundi[0];
		document.querySelector(".lundiContainer .plat").textContent = objetMenu.lundi[1];
		document.querySelector(".lundiContainer .dessert").textContent = objetMenu.lundi[2];
		document.querySelector(".mardiContainer .entree").textContent = objetMenu.mardi[0];
		document.querySelector(".mardiContainer .plat").textContent = objetMenu.mardi[1];
		document.querySelector(".mardiContainer .dessert").textContent = objetMenu.mardi[2];
		document.querySelector(".mercrediContainer .entree").textContent = objetMenu.mercredi[0];
		document.querySelector(".mercrediContainer .plat").textContent = objetMenu.mercredi[1];
		document.querySelector(".mercrediContainer .dessert").textContent = objetMenu.mercredi[2];
		document.querySelector(".jeudiContainer .entree").textContent = objetMenu.jeudi[0];
		document.querySelector(".jeudiContainer .plat").textContent = objetMenu.jeudi[1];
		document.querySelector(".jeudiContainer .dessert").textContent = objetMenu.jeudi[2];
		document.querySelector(".vendrediContainer .entree").textContent = objetMenu.vendredi[0];
		document.querySelector(".vendrediContainer .plat").textContent = objetMenu.vendredi[1];
		document.querySelector(".vendrediContainer .dessert").textContent = objetMenu.vendredi[2];
	}

	var menusObj;

	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/selectDB.php?toGet=menus');

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			menusObj =  JSON.parse(xhr.responseText);
			remplirMenu(menusObj);
		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};

	xhr.send(null);

})();

//Remplissage du suggestions
(function() {

	var remplirSuggestions = function(objetSuggestions){
		var suggestionsContainer = document.querySelector('.suggestionsContainer');

		for(var i = 0, c = objetSuggestions.length; i < c; i++) {
			var tempNode = document.createElement('div');
			tempNode.className = '';
			var tempNomPlat = document.createElement('div');
			tempNomPlat.innerText = objetSuggestions[i][0];
			tempNomPlat.textContent = objetSuggestions[i][0];
			tempNomPlat.className = 'nom';
			var tempPrix = document.createElement('div');
			tempPrix.innerText = objetSuggestions[i][1] + ' .-';
			tempPrix.textContent = objetSuggestions[i][1] + ' .-';
			tempPrix.className = 'prix';
			tempNode.appendChild(tempNomPlat);
			tempNode.appendChild(tempPrix);
			suggestionsContainer.appendChild(tempNode);
		}
	}

	var menusObj;

	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/selectDB.php?toGet=suggestions');

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			menusObj =  JSON.parse(xhr.responseText);
			remplirSuggestions(menusObj);
		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};

	xhr.send(null);

})();

//Remplissage de la carte
(function() {

	var remplirCarte = function(objetCarte){
		var entreeContainer = document.querySelector('.carteEntreeContainer');

		for(var i = 0, c = objetCarte['entree'].length; i < c; i++) {
			var tempNode = document.createElement('div');
			tempNode.className = 'containerLigneCarte';
			var tempNomPlat = document.createElement('div');
			tempNomPlat.className = 'nom';
			tempNomPlat.innerText = objetCarte['entree'][i][0];
			tempNomPlat.textContent = objetCarte['entree'][i][0];
			var tempPrix = document.createElement('div');
			tempPrix.innerText = objetCarte['entree'][i][1] + ' .-';
			tempPrix.textContent = objetCarte['entree'][i][1] + ' .-';
			tempPrix.className = 'prix';
			tempNode.appendChild(tempNomPlat);
			tempNode.appendChild(tempPrix);
			entreeContainer.appendChild(tempNode);
		}

		var platContainer = document.querySelector('.cartePlatContainer');

		for(var i = 0, c = objetCarte['plat'].length; i < c; i++) {
			var tempNode = document.createElement('div');
			tempNode.className = 'containerLigneCarte';
			var tempNomPlat = document.createElement('div');
			tempNomPlat.className = 'nom';
			tempNomPlat.innerText = objetCarte['plat'][i][0];
			tempNomPlat.textContent = objetCarte['plat'][i][0];
			var tempPrix = document.createElement('div');
			tempPrix.innerText = objetCarte['plat'][i][1] + ' .-';
			tempPrix.textContent = objetCarte['plat'][i][1] + ' .-';
			tempPrix.className = 'prix';
			tempNode.appendChild(tempNomPlat);
			tempNode.appendChild(tempPrix);
			platContainer.appendChild(tempNode);
		}

		var dessertContainer = document.querySelector('.carteDessertContainer');

		for(var i = 0, c = objetCarte['dessert'].length; i < c; i++) {
			var tempNode = document.createElement('div');
			tempNode.className = 'containerLigneCarte';
			var tempNomPlat = document.createElement('div');
			tempNomPlat.className = 'nom';
			tempNomPlat.innerText = objetCarte['dessert'][i][0];
			tempNomPlat.textContent = objetCarte['dessert'][i][0];
			var tempPrix = document.createElement('div');
			tempPrix.innerText = objetCarte['dessert'][i][1] + ' .-';
			tempPrix.textContent = objetCarte['dessert'][i][1] + ' .-';
			tempPrix.className = 'prix';
			tempNode.appendChild(tempNomPlat);
			tempNode.appendChild(tempPrix);
			dessertContainer.appendChild(tempNode);
		}
	}

	var menusObj;

	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/selectDB.php?toGet=carte');

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			menusObj =  JSON.parse(xhr.responseText);
			remplirCarte(menusObj);
		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};

	xhr.send(null);

})();

$('#tabMenu a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})

$('#tabsuggestions a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})

$('#tabscarte a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})

$('#tabBoissons a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})


$('#tabInfosPratiques a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})


$('#tabBanquets a').click(function (e) {
	e.preventDefault()
	$(this).tab('show')
})

/*
for(var i = 0, c = menuComp.plat.length; i < c; i++) {
	var tempElem = document.createElement('div');
	tempElem.className = 'menuItem';
	tempElem.innerHTML = menuComp.plat[i];
	menuDiv.appendChild(tempElem);
}
*/