//A Faire, la sécurité : empéché de pouvoir mettre à jour en différé quand une mise à jour en différé est déja prévu (caché le bouton mettre à jour). Faire ça dans la fonction displayAndFill
//A Faire, terminer l'API de mise à jour en différé
//OPTIONEL, griser le bouton de mise à jour après qu'il ait été cliqué

$(document).ready(function () {
	checkEvent();
});

var indexField = 0;

//Remplissage
(function(){
	var menusObj;

	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/selectDB.php?toGet=menus');

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			objetMenu =  JSON.parse(xhr.responseText);

			document.querySelector(".lundiContainer .entree").value = objetMenu.lundi[0];
			document.querySelector(".lundiContainer .plat").value = objetMenu.lundi[1];
			document.querySelector(".lundiContainer .dessert").value = objetMenu.lundi[2];
			document.querySelector(".mardiContainer .entree").value = objetMenu.mardi[0];
			document.querySelector(".mardiContainer .plat").value = objetMenu.mardi[1];
			document.querySelector(".mardiContainer .dessert").value = objetMenu.mardi[2];
			document.querySelector(".mercrediContainer .entree").value = objetMenu.mercredi[0];
			document.querySelector(".mercrediContainer .plat").value = objetMenu.mercredi[1];
			document.querySelector(".mercrediContainer .dessert").value = objetMenu.mercredi[2];
			document.querySelector(".jeudiContainer .entree").value = objetMenu.jeudi[0];
			document.querySelector(".jeudiContainer .plat").value = objetMenu.jeudi[1];
			document.querySelector(".jeudiContainer .dessert").value = objetMenu.jeudi[2];
			document.querySelector(".vendrediContainer .entree").value = objetMenu.vendredi[0];
			document.querySelector(".vendrediContainer .plat").value = objetMenu.vendredi[1];
			document.querySelector(".vendrediContainer .dessert").value = objetMenu.vendredi[2];

		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};

	xhr.send(null);
})();


//Création et remplissage des champs de la partie SUGGESTIONS
(function(){
	var menusObj;

	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/selectDB.php?toGet=suggestions');

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			menusObj =  JSON.parse(xhr.responseText);

			for(var i = 0, c = menusObj.length; i < c; i++) {
				addField('.suggestionsContainer', menusObj[i][0], menusObj[i][1]);
			};

		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};

	xhr.send(null);
})();

//Création et remplissage des champs de la partie CARTE
(function(){
	var menusObj;

	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/selectDB.php?toGet=carte');

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			menusObj =  JSON.parse(xhr.responseText);

			for(var i = 0, c = menusObj['entree'].length; i < c; i++) {
				addField('.entreesContainer', menusObj['entree'][i][0], menusObj['entree'][i][1], 'entree');
			};

			for(var i = 0, c = menusObj['plat'].length; i < c; i++) {
				addField('.platsContainer', menusObj['plat'][i][0], menusObj['plat'][i][1], 'plat');
			};

			for(var i = 0, c = menusObj['dessert'].length; i < c; i++) {
				addField('.dessertsContainer', menusObj['dessert'][i][0], menusObj['dessert'][i][1], 'dessert');
			};

		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};

	xhr.send(null);
})();

addField = function(emplacement, nom_plat, prix, plat) {

	nom_plat = nom_plat || '-';
	prix = prix || 2;

	var whereToPlace = document.querySelector(emplacement);

	if(plat) {
		var hidden = document.createElement('input');
		hidden.setAttribute('name', 'type');
		hidden.setAttribute('type', 'hidden');
		hidden.value = plat;
		hidden.className = 'cl' + indexField;
		whereToPlace.appendChild(hidden);
	}

	var tempElement = document.createElement("input");
	tempElement.setAttribute('name', 'nom_plat');
	tempElement.setAttribute('type', 'text');
	tempElement.value = nom_plat;
	tempElement.className = 'cl' + indexField;
	whereToPlace.appendChild(tempElement);

	var tempPrix = document.createElement('input');
	tempPrix.setAttribute('name', 'prix');
	tempElement.setAttribute('type', 'text');
	tempPrix.value = prix;
	tempPrix.className = 'prix ' +  'cl' + indexField;
	whereToPlace.appendChild(tempPrix);

	var tempButton = document.createElement("button");
	tempButton.innerText = "Supprimer";
	tempButton.textContent = "Supprimer";
	tempButton.className = 'cl' + indexField + ' preventDefault';
	tempButton.setAttribute('onclick', "deleteField('.cl" + indexField + "')");
	whereToPlace.appendChild(tempButton);
	indexField++;
}

Element.prototype.remove = function() {
	this.parentElement.removeChild(this);
}

deleteField = function(fieldToDelete) {
	var elementsToDelete = document.querySelectorAll(fieldToDelete);

	for (var i = 0; i < elementsToDelete.length; i++) {
		elementsToDelete[i].remove();
	};
}

udpadeFromForm = function(form){

	var objForm = $(form).serializeArray(), i = 0, objFormData = new FormData();
	console.log(objForm);

	for(var i = 0, c = objForm.length; i<c; i++) {
		if(objForm[i].name != 'date'){
			objFormData.append(i, objForm[i].value);
		}
	}

	//On commence par instancier l'objet XHR
	var xhr = new XMLHttpRequest();

	//On prépare la requete avec la méthode open(methode d'envoie, URL où envoyer la requête, true: asynchrone / false: synchrone, nom d'utilisateur, mot de passe);
	xhr.open('POST', 'http://localhost/Raisin_cafe/API/updateDB.php');

	//On envoie ensuite la requête avec la méthode send()
	xhr.send(objFormData);
};

udpadeFromFormDelayed = function(form){

	var objForm = $(form).serializeArray(), i = 0, objFormData = new FormData();

	console.log(objForm);
	console.log(objForm[objForm.length-1].value);

	objFormData.append('0', objForm[objForm.length-1].value);

	for(var i = 0, c = objForm.length-1; i<c; i++) {
		if(objForm[i].name != 'date'){
			objFormData.append(i+1, objForm[i].value);
		}
	}

	//On commence par instancier l'objet XHR
	var xhr = new XMLHttpRequest();

	//On prépare la requete avec la méthode open(methode d'envoie, URL où envoyer la requête, true: asynchrone / false: synchrone, nom d'utilisateur, mot de passe);
	xhr.open('POST', 'http://localhost/Raisin_cafe/API/updateDBtimer.php');

	//On envoie ensuite la requête avec la méthode send()
	xhr.send(objFormData);
};

displayAndFillEvents = function(eventsUp) {
	var eventMenus = document.querySelector('#eventMenus');
	var eventSuggestions = document.querySelector('#eventSuggestions');
	var eventCarte = document.querySelector('#eventCarte');

	eventMenus.className = 'hidden';
	eventSuggestions.className = 'hidden';
	eventCarte.className = 'hidden';

	for(var i = 0, c = eventsUp.length; i < c; i++) {
		if(eventsUp[i].name == 'menuEventInsert') {
			eventMenus.className = "";
			document.querySelector('#eventMenus .dateEvent').innerText = eventsUp[i].date;
			document.querySelector('#eventMenus .dateEvent').textContent = eventsUp[i].date;
		}
		if(eventsUp[i].name == 'suggestionEventInsert') {
			eventSuggestions.className = "";
			document.querySelector('#eventSuggestions .dateEvent').innerText = eventsUp[i].date;
			document.querySelector('#eventSuggestions .dateEvent').textContent = eventsUp[i].date;
		}
		if(eventsUp[i].name == 'carteEventInsert') {
			eventCarte.className = "";
			document.querySelector('#eventCarte .dateEvent').innerText = eventsUp[i].date;
			document.querySelector('#eventCarte .dateEvent').textContent = eventsUp[i].date;
		}
	}
}

//Grosse fonction qui s'occupe de tous les event, elle demande au serveur les evenements prévus
//ensuite elle fait appraitre les div des evenements
checkEvent = function () {

	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/selectDB.php?toGet=events');

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			eventsObj =  JSON.parse(xhr.responseText);

			displayAndFillEvents(eventsObj);

		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};

	xhr.send(null);
}

//fonction qui supprime les evenements qu'on lui donne (sous forme de tableau)
deleteEvent = function(eventsToDelete) {
	var xhr = new XMLHttpRequest();

	xhr.open('GET', 'http://localhost/Raisin_cafe/API/deleteEvent.php?0=' + eventsToDelete[0] + '&1=' + eventsToDelete[1]);

	xhr.send(null);

	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {

			checkEvent();

		} else if (xhr.readyState == 4 && xhr.status != 200) {
			alert('Une erreur est survenue \n\nCode : ' + xhr.status + '\nTexte : ' + xhr.statusText);
		}
	};
}

var buttonUpdate = document.querySelectorAll('.preventDefault');

for (var i = 0, c = buttonUpdate.length; i<c; i++) {

	buttonUpdate[i].addEventListener('click', function(e){
		if(e.preventDefault){
			e.preventDefault();
		}
	}, false)
}


