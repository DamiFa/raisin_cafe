<?php 

header('Content-type: text/html; charset=UTF-8');

try {
	$bdd = new PDO ('mysql:host=localhost;dbname=mfiz_raisin_cafe', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch (Exception $e) {
	die("Erreur : " . $e->getMessage());
}

$i = 1;
if($_POST[0] == 'suggestions') {
	$query = "DELETE FROM suggestion; INSERT INTO suggestion (id, nom_plat, prix) VALUES ";
	while ($_POST[$i]) {
		$query.= "(" . "'null','". utf8_encode($_POST[$i++]) ."','". $_POST[$i++] ."')";
		if($_POST[$i+1]) {
			$query.=',';
		}
	}
} elseif ($_POST[0] == 'carte') {
	$query = "DELETE FROM carte; INSERT INTO carte (id, plat, nom_plat, prix) VALUES ";
	while ($_POST[$i]) {
		$query.= "(" . "'null','". $_POST[$i++] ."','". utf8_encode($_POST[$i++]) ."','". $_POST[$i++] ."')";
		if($_POST[$i+1]) {
			$query.=',';
		}
	}
} elseif ($_POST[0] == 'menu') {
	$query = "DELETE FROM menu; INSERT INTO menu (id, jour, plat, nom_plat) VALUES ";
	while ($_POST[$i]) {
		$query.= "(" . "'null','". $_POST[$i++] ."','". $_POST[$i++] ."','". utf8_encode($_POST[$i++]) ."')";
		if($_POST[$i+1]) {
			$query.=',';
		}
	}
}

echo $query;

$bdd->exec($query);

?>