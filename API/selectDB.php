<?php 

header('Content-Type: text/html; charset=ISO-8859-1');

try {
	$bdd = new PDO ('mysql:host=localhost;dbname=mfiz_raisin_cafe', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch (Exception $e) {
	die("Erreur : " . $e->getMessage());
}

$encode = array();


if($_GET['toGet'] == 'menus') {

	$reponse = $bdd->query("SELECT jour, plat, nom_plat FROM menu");

	while($donneesI = $reponse->fetch(PDO::FETCH_ASSOC)) {
		$encode[$donneesI['jour']][] = utf8_decode($donneesI['nom_plat']);
	}
} else if ($_GET["toGet"] == 'suggestions') {
	$reponse = $bdd->query("SELECT nom_plat,prix FROM suggestion");

	while($donneesI = $reponse->fetch()) {
		$encode[] = array(utf8_decode($donneesI['nom_plat']), $donneesI['prix']);
	}
} else if ($_GET["toGet"] == 'carte' ) {
	$reponse = $bdd->query("SELECT nom_plat, plat, prix FROM carte");

	while($donneesI = $reponse->fetch()) {
		$encode[$donneesI["plat"]][] = array(utf8_decode($donneesI['nom_plat']), $donneesI['prix']);
	}
} elseif ($_GET['toGet'] == 'events') {
	$reponse = $bdd->query("SHOW EVENTS");

	while($donneesI = $reponse->fetch(PDO::FETCH_ASSOC)) {
		$encode[] = array("name" => utf8_decode($donneesI["Name"]),"date" => utf8_decode($donneesI["Execute at"]));
	}
}

print_r (json_encode($encode));


?>
